// main_config.h

#ifndef MAIN_CONFIG_H_INCLUDED
#define MAIN_CONFIG_H_INCLUDED

#include "trigger.h"

//Compile flags - which keys should be included 1: use, 0: don't use aditional keys
//Disabling some features will reduce program size - may even fit to Atiny45?
#define KEYS_LETTERS_ALL            1       //a-z
#define KEYS_LETTERS_CAPITAL_ALL    1       //A-Z
#define KEYS_NUMBERS_ALL            1       //0-9
#define KEYS_ASCII_SOME             1       //# / _ @
#define KEYS_ASCII_ALL              1       //ASCII_SOME has to be 1 for this to have an effect
#define KEYS_CONTROL_ALL            1       //arrow key, pgUp, pgDown,...
#define KEYS_FKEYS_ALL              1       //F1-F12
#define KEYS_CZ_KEYS_ALL            1       //Ability to use ˇ and ' for ěščřžýáíé (has to be send as sequence ˇz to create ž)
#define KEYS_HOST_OS                1       //Specify for which OS this device is used - valid values: 0:None 1:LINUX, 2:WINDOWS - see end of hid.c file

#define PRINT_ENABLED               1       //Enable printing function over USB keyboard HID protocol

#define HWLED_CONNECTED             1       //Hardware LED is connected to indicate device's operation
#define HWLED_P                     PB1     //Since ATtiny85 has only PORTB it is not set here - see below
#define HWLED_OPERATION_INVERTED    0       //0:normaly OFF, turns ON when sending data; 1:inverted; Can be also useful when LED is connected in oposit direction
#define HWLED_CONTROL_LED           1       //Enable hardware LED control through USB protocol

//Prepare data storage in eeprom (DATA) with corresponding generic buffer (it should not be smaller than 3 since it is also used for configuration data -> risk of accessing memory outside buffer)
#define DATA_COUNT                  8       //Number of data blocks in eeprom
#define DATA_EEPROM                 63      //Minimal EEPROM storage spece necessary for 1 data set

//------------------------------------------------------------------------------
//---Dependent compiler settings------------------------------------------------
//------------------------------------------------------------------------------

//Hardware connection of LED
#define HWLED_DDR                   DDRB
#define HWLED_PORT                  PORTB

//EEPROM storage is formed as (1 byte (trigger length) + TRIGGER_SIZE bytes (array of bytes representing trigger signal) + N-bytes (actual stored data)) == DATA_EEPROM
#define DATA_MAX_SIZE               (DATA_EEPROM - 1 - TRIGGER_SIZE)        //Length of a single eeprom memory block (DATA_MAX_SIZE=<BUFFER_SIZE) - don't change
#if DATA_MAX_SIZE <= 1
    #error DATA_MAX_SIZE is too small
#endif // DATA_MAX_SIZE
#define BUFFER_SIZE                 (DATA_MAX_SIZE+1)   //Generic buffer for data handling (+1 means 1st byte is storage address where the incoming message from host should be stored)

#endif // MAIN_CONFIG_H_INCLUDED
