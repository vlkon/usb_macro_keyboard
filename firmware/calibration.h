#ifndef CALIBRATION_H_INCLUDED
#define CALIBRATION_H_INCLUDED

#define abs(x) ((x) > 0 ? (x) : (-x))
//Calibration constants are higly influenced by used device (one attiny45 was 155, attiny85 was 135) has to be checked for each MCU
//Using full range scan cause problems with some OS (timeout -> device not detected)
#define CALIB_START             105     //Middle of calibration interval range (see datasheet for Attiny45/85) - chip dependent
#define CALIB_INTERVAL_SIZE     64      //Actually it will be 63 as -16-8-4-2-1=-21 and +16+8+4+2+1==+21

void hadUsbReset();

#endif // CALIBRATION_H_INCLUDED
