// main.c

/*----------------------*/
/*--Includes------------*/
/*----------------------*/

#include <avr/io.h>
#include <avr/wdt.h>        //watch dog timer for handling CPU freezes
#include <avr/interrupt.h>  //for sei() an all interrupt driven events
#include <util/delay.h>     //for _delay_ms()
#include <avr/sleep.h>      //Power saving mode

#include <avr/eeprom.h>     //store data string (password)
#include <avr/pgmspace.h>
#include <string.h>

#include <stdlib.h>         //for itoa()

#include "main.h"
#include "usbdrv/usbdrv.h"  //main v-usb interface
#include "timer.h"          //50Hz delay routine and "USB is alive" protocol

#include "hid.h"            //HID keyboard interface - also contains some declared global variables
#include "hid_keys.h"       //Defines for keyboard layout and commands

#include "hw_led.h"         //hardware LED for status signaling
#include "trigger.h"        //procedures to pass trigger signals into the main loop
#include "button.h"

/*----------------------*/
/*--Global storage------*/
/*----------------------*/

//Global flags
generalConfig_struct_t generalConfig;                   //Structure holding information for lock-LED send msg trigger event

//Custom defines for device memory storage
//Prepare data storage
#if BUFFER_SIZE>64
#error "Maximal buffer size is 63+NULL (there may be limit on USB transfer protocol - not confirmed)"
#endif // BUFFER_SIZE
//Allocating memory space for passwords - can be also done in a single struct
//Each block has to be NULL terminated string of uint8_t
EEMEM uint8_t eeData[DATA_COUNT][DATA_EEPROM] = {{0}};

//Num-lock trigger event configuration data
//{reserved==0xFF(eeprom default value), triggerCountTimeout (1s), dataSendDelay (0.3s), buttonACntr (40ms), button2Cntr (40ms)}
//reserved 0xFF should be at the beginning of eeprom memory (check *.eep file) - sometimes it can be corrupted due to power failure - maybe it is useless in this application
EEMEM generalConfig_struct_t eeGeneralConfig = {0xFF, 0x32, 0x0F, 0x05, 0x02, 0x02};

//Generic global data buffer
uint8_t buffer[BUFFER_SIZE] = {0};

uint8_t dataTrigger[DATA_COUNT * (TRIGGER_SIZE + 1)] = {0};    //+1 stores size of trigger array

#if PRINT_ENABLED == 1
//Print whole eeprm memory
void printMemory(void);
//Envelope function for formating printing function regarding Config eeprom data block
void printConfig(PGM_P pDes, uint8_t* data, uint8_t dataLen);
//Convert data from memory buffer into printable format; ascii==0: print all in hex format, 1: print printable characters + non-printable hex values
void printConvertData(const uint8_t* dataBlock, uint8_t len, uint8_t ascii);
//Convert uint8_t value into decimal value in 3 digit format 000
void decKey(const uint8_t data, uint8_t* buf);
//Convert uint8_t value into hex value in 3 digit format #00
void hexKey(const uint8_t data, uint8_t* buf);

PROGMEM const char prtCfg0[] = {"Config"};
PROGMEM const char prtCfg1[] = {"_Counter_timeout___"};
PROGMEM const char prtCfg2[] = {"_Delay_after_trigger___"};
PROGMEM const char prtCfg3[] = {"_Button_All_reset_counter___"};
PROGMEM const char prtCfg4[] = {"_Button_A_counter___"};
PROGMEM const char prtCfg5[] = {"_Button_B_counter___"};
PROGMEM const char prtClck[] = {"Clock_calibration___"};
PROGMEM const char prtMem[]  = {"Memory_block___"};
PROGMEM const char prtTrs[]  = {"_Trigger_size___"};
PROGMEM const char prtTrb[]  = {"_Trigger_block___"};
#endif // PRINT_ENABLED


/*----------------------*/
/*--- Main functions ---*/
/*----------------------*/
int main(void)
{
    uint8_t iii;                        //generic iterator variable

    #if HWLED_CONNECTED == 1
    hwLedInit();                        //Set default state of hardware indicator LED
    #endif // HWLED_CONNECTED

    wdt_enable(WDTO_2S);                //Enable watchdog for 1s intervals - resets are called in Timer1 ISR
    set_sleep_mode(SLEEP_MODE_IDLE);    //Need timer1 interrupt

    keyboardReportReset();              //Define initial values for keyboard report
    usbInit();                          //Initialize USB protocols

    uint8_t timeOut = 30;
    usbDeviceDisconnect();              //Enforce re-enumeration, do this while interrupts are disabled!

    while(--timeOut) {                  //Fake USB disconnect for > 250 ms
        wdt_reset();                    //Prevent reset from watchdog
        _delay_ms(10);
    }

    usbDeviceConnect();

    buttonInit();
    timerInit();                        //USB polling and timeout handling
    sei();
    //All above code has to be within watch-dog timer. It shouldn't be a problem even at 12 MHz clock and 1s of WDT.

    eepromInit();

    //indication that the device was turned on
    #if HWLED_CONNECTED == 1
    hwLedOn();
    timeOut = 10;
    #endif // HWLED_CONNECTED

    while(--timeOut) {
        wdt_reset();
        sleep_mode();
    }

    #if HWLED_CONNECTED == 1
    hwLedOff();
    #endif // HWLED_CONNECTED


    // main event loop
    while(1) {
        switch(trigger[0]) {
            case TRIGGER_IDLE:
                //Do nothing if no control flag is set - wait for next cycle of timer1
                wdt_reset();
                sleep_mode();
                break;

            case TRIGGER_SEND_DATA:
                //Send a data block from eeprom based on received signal + address (vendor signal over USB)
                //It is not printed with keyboard HID interface
                dataSend(trigger[1]);
                break;

            case TRIGGER_BULK_DATA_RECEIVED:
                //Store received data into eeprom on a given address
                dataStore();
                break;

            #if PRINT_ENABLED ==1
            case TRIGGER_PRINT_MEMORY:
                //Dump whole memory report using keyboard HID interface
                delayTimer1(0xFF);      //Long delay to allow user to switch into text editor to receive message    //TODO - may be changed in future
                printMemory();
                break;
            #endif // PRINT_ENABLED

            default:
                //Compare trigger to eeprom memory triggers to determine if a specific memory block should be printed.
                //Using keyboard HID interface
                for(iii = 0; iii < DATA_COUNT; ++iii) {
                    if(triggerIsEqual(trigger, &dataTrigger[iii*(TRIGGER_SIZE+1) + 1], dataTrigger[iii*(TRIGGER_SIZE+1)])) {
                        dataSend(iii);
                        triggerReset();
                        continue;
                    }
                }
                break;
        }
    }
}


//Software reset
void mcuReset()
{
    cli();          //Disable interupts - do not interfere with reseting MCU
    while(1) {};    //Enter infinite loop and wait for watchdog to reach its reset condition
}


void eepromInit(void)
{
    uint8_t iii;
    for(iii=0; iii<DATA_COUNT; ++iii){
        eeprom_read_block(&dataTrigger[iii * (TRIGGER_SIZE + 1)], &eeData[iii][0], TRIGGER_SIZE + 1);
    }
    eeprom_read_block(&generalConfig, &eeGeneralConfig, sizeof(generalConfig_struct_t));
}


//Decypher data stored in eeprom
uint8_t parseKeyBuffer(uint8_t* buf, const uint8_t maxLen)
{
    lockDefault();                                                  //Set num- caps-lock to value ON, OFF respectively

    uint8_t cntr;                                                   //Generic loop counter

    for(cntr = 0; cntr < maxLen; ++cntr) {
        wdt_reset();                                                //Reset WDT for possibly long cycle

        if(buf[cntr] == 0x00) {
            return 0x00;                                            //Proper return - NULL terminated buffer

        } else if(buf[cntr] < 0x80) {                                //Control characters 1-31, Ascii "visible" characters 32-127
            sendKeyPress(buf[cntr]);

        #if KEYS_HOST_OS!=0
        } else if(buf[cntr] == KEY_CTRL_ALT_DEL) {                   // Send Crtl+Alt+Del signal - especially useful on windows
            sendKeyPress(buf[cntr]);
        #endif // KEYS_HOST_OS

        } else {                         //Special "undefined" Ascii characters - used for device handling (e.g. delays between key presses - stored in eeprom data)
            //Warning! - collision with other signals may happen if they are implemented in range 0x80-0xFF

            switch(buf[cntr]) {
                case DELAY_STR_100MS:
                    delayTimer1((uint8_t)(F_TIMER1 * 1 / 10));      //100ms delay with 50Hz timer
                    break;

                case DELAY_STR_500MS:
                    delayTimer1((uint8_t)(F_TIMER1 * 5 / 10));      //500ms delay with 50Hz timer
                    break;

                case DELAY_STR_1000MS:
                    delayTimer1((uint8_t)(F_TIMER1 * 1));           //1000ms delay with 50Hz timer
                    break;

                case DELAY_STR_2000MS:
                    delayTimer1((uint8_t)(F_TIMER1 * 2));           //2000ms delay with 50Hz timer
                    break;

                case DELAY_STR_3000MS:
                    delayTimer1((uint8_t)(F_TIMER1 * 3));           //3000ms delay with 50Hz timer
                    break;
            }
        }
    }

    return 0x01;       //Not a NULL terminated buffer - error
}


//Send data from eeprom to USB pipe
void dataSend(const uint8_t address)
{
    #if HWLED_CONNECTED == 1
    hwLedOn();
    #endif // HWLED_CONNECTED

    delayTimer1(generalConfig.dataSendDelay);                          //Add delay - user stops toggling lock-LEDs and HID driver hooks up back to the device

    eeprom_read_block(buffer, eeData[address] + TRIGGER_SIZE + 1, DATA_MAX_SIZE);      //Read - data block 0 from eeprom - only one block avalable for num lock trigger

    if(parseKeyBuffer(buffer, DATA_MAX_SIZE)) {                     //Send content of buffer to HID keyboad interface
        mcuReset();
    }

    triggerReset();
    //triggerTimeout = 0;                                           //Can be left on ISR to reset it back to 0. Saves 4 bytes of flash memory

    #if HWLED_CONNECTED == 1
    hwLedOff();
    #endif // HWLED_CONNECTED
}


//Store received data to proper eeprom memory location
void dataStore()
{
    #if HWLED_CONNECTED == 1
    hwLedOn();
    #endif // HWLED_CONNECTED

    if(buffer[0] == 0xFF) {
        //Store lock key trigger configuration data
        generalConfig.triggerCountTimeout   = buffer[1];
        generalConfig.dataSendDelay         = buffer[2];
        generalConfig.buttonAllCntr         = buffer[3];
        generalConfig.buttonACntr           = buffer[4];
        generalConfig.buttonBCntr           = buffer[5];

        eeprom_update_block(&generalConfig, &eeGeneralConfig, sizeof(generalConfig_struct_t));

    } else if(buffer[0] < DATA_COUNT) {                     //Check for valid eemem address
        // Store keyboard macro at designated memory location
        eeprom_update_block((buffer + 1), eeData[buffer[0]] + TRIGGER_SIZE + 1, DATA_MAX_SIZE); //buffer+1 means that first possition of buffer stores information on memory location; for TRIGGER_SIZE+1 see #define DATA_MAX_SIZE

    } else if((buffer[0] >= 100) &&
              ((buffer[0] - 100) < DATA_COUNT)) {       //Check for a trigger condition
        // Store trigger length and trigger signal in 1 operation
        eeprom_update_block((buffer + 1), eeData[buffer[0] - 100], TRIGGER_SIZE);
    }

    #if HWLED_CONNECTED == 1
    hwLedOff();
    #endif // HWLED_CONNECTED

    mcuReset();
}


//Printing functions-----------
#if PRINT_ENABLED == 1
void printMemory()
{
    #if HWLED_CONNECTED == 1
    hwLedOn();
    #endif // HWLED_CONNECTED

    //Generic loop iterators
    uint8_t iii;
    uint8_t jjj;

    //Generic config settings
    printConfig(prtCfg0, 0, 0);
    printConfig(prtCfg1, (uint8_t*)&generalConfig.triggerCountTimeout, 1);
    printConfig(prtCfg2, (uint8_t*)&generalConfig.dataSendDelay, 1);
    printConfig(prtCfg3, (uint8_t*)&generalConfig.buttonAllCntr, 1);
    printConfig(prtCfg4, (uint8_t*)&generalConfig.buttonACntr, 1);
    printConfig(prtCfg5, (uint8_t*)&generalConfig.buttonBCntr, 1);

    //Get calibration value from oscillator
    iii = OSCCAL;                               //borrow some not yet used variable
    sendKeyPress(KEY_RETURN);
    printConfig(prtClck, &iii, 1);

    //Print all data eeprom memory blocks
    for(iii = 0; iii < DATA_COUNT; ++iii) {

        // Send memory number
        sendKeyPress(KEY_RETURN);
        strcpy_P((char*)buffer, prtMem);
        printConvertData(buffer, strlen((char*)buffer), 1);

        decKey(iii, buffer);

        for(jjj = 0; jjj < 3; ++jjj) {
            sendKeyPress(buffer[jjj]);
        }

        // Send trigger buffer length
        sendKeyPress(KEY_RETURN);
        strcpy_P((char*)buffer, prtTrs);
        printConvertData(buffer, strlen((char*)buffer), 1);
        eeprom_read_block(buffer, eeData[iii] , 1);
        printConvertData(buffer, 1, 0);

        // Send trigger buffer content
        sendKeyPress(KEY_RETURN);
        strcpy_P((char*)buffer, prtTrb);
        printConvertData(buffer, strlen((char*)buffer), 1);
        eeprom_read_block(buffer, eeData[iii] + 1 , TRIGGER_SIZE);
        printConvertData(buffer, TRIGGER_SIZE, 0);

        // Send content of eeprom data block
        sendKeyPress(KEY_RETURN);
        eeprom_read_block(buffer, eeData[iii] + TRIGGER_SIZE +1 , DATA_MAX_SIZE);
        printConvertData(buffer, DATA_MAX_SIZE, 1);     // ascii values
        sendKeyPress(KEY_RETURN);
        printConvertData(buffer, DATA_MAX_SIZE, 0);     // raw hex values of the memory
        sendKeyPress(KEY_RETURN);
    }

    triggerReset();
    #if HWLED_CONNECTED == 1
    hwLedOff();
    #endif // HWLED_CONNECTED
}


//Envelope function for formating printing function regarding Config eeprom data block
void printConfig(PGM_P pDes, uint8_t* data, uint8_t dataLen)
{
    strcpy_P((char*)buffer, pDes);
    printConvertData(buffer, strlen((char*)buffer), 1);
    if(dataLen) {
        printConvertData(data, dataLen, 0);
    }
    sendKeyPress(KEY_RETURN);
}


void printConvertData(const uint8_t* dataBlock, uint8_t len, uint8_t ascii)
{
    #define _TEMP_BUF_SIZE      4       //Has to be 4 or larger (3 digit+null terminator)
    uint8_t iii;
    uint8_t jjj;
    uint8_t tempBuf[_TEMP_BUF_SIZE] = {0};

    for(iii = 0; iii < len; ++iii) {
        wdt_reset();                            //Reset WDT for possibly long cycle

        if((ascii == 1) &&
           (dataBlock[iii] < 0x7F) &&
           (dataBlock[iii] > 0x20)) {
            sendKeyPress(dataBlock[iii]);       //Print ascii if possible
        } else if((ascii == 1) &&
                  (dataBlock[iii] == 0x00)) {
            return;                             //In ascii mode end at null termination
        } else {
            hexKey(dataBlock[iii], tempBuf);

            for(jjj = 0; jjj < _TEMP_BUF_SIZE; ++jjj) {
                sendKeyPress(tempBuf[jjj]);
            }
        }
    }
    #undef _TEMP_BUF_SIZE
}


void decKey(const uint8_t data, uint8_t* buf)
{
    #if KEYS_NUMBERS_ALL != 1
    #error "This function requires numbers to be defined in keyboard code."
    #endif // KEYS_NUMBERS_ALL

    if(data >= 100) {
        utoa(data, (char*)&buf[0], 10);
    } else if(data >= 10) {
        utoa(data, (char*)&buf[1], 10);
        buf[0] = '0';
    } else {
        utoa(data, (char*)&buf[2], 10);
        buf[0] = '0';
        buf[1] = '0';
    }
}


void hexKey(const uint8_t data, uint8_t* buf)
{
    #if KEYS_NUMBERS_ALL != 1
    #error "This function requires numbers to be defined in keyboard code."
    #endif // KEYS_NUMBERS_ALL

    #if KEYS_LETTERS_ALL != 1
    #error "This function requires lowercase letters to be defined in keyboard code."
    #endif // KEYS_NUMBERS_ALL

    #if KEYS_ASCII_SOME != 1
    #error "This function requires special ASCII character to be defined in keyboard code."
    #endif // KEYS_NUMBERS_ALL

    buf[0] = '#';

    if(data > 0x0F) {
        utoa(data, (char*)&buf[1], 16);
    } else {
        utoa(data, (char*)&buf[2], 16);
        buf[1] = '0';
    }
}
#endif // PRINT_ENABLED
