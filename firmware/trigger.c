#include <stdint.h>

#include "trigger.h"
#include "main.h"

volatile uint8_t trigger[TRIGGER_SIZE];
volatile uint8_t triggerTimeout = 0;
static volatile uint8_t triggerPointer = 0;     // Points to current "free" position in trigger array

void triggerReset()
{
    uint8_t iii;
    for(iii = 0; iii < TRIGGER_SIZE; ++iii) {
        trigger[iii] = TRIGGER_IDLE;
    }

    triggerPointer = 0;
}

uint8_t triggerIsEqual(volatile uint8_t* arrA, uint8_t* arrB, uint8_t trigger_length)
{
    if(trigger_length > TRIGGER_SIZE || trigger_length == 0) {
        // Discard calls where length of compared arrays is out of expected bounds
        return 0;
    }

    uint8_t iii;
    for(iii = 0; iii < trigger_length; ++iii) {
        if(arrA[iii] != arrB[iii]) {
            return 0;        //not equal
        }
    }

    return 1;   //is equal
}

void triggerAdd(uint8_t triggerSignal)
{
    trigger[triggerPointer] = triggerSignal;
    ++triggerPointer;
    triggerTimeout = generalConfig.triggerCountTimeout;
}
