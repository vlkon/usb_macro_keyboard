#ifndef TRIGGER_H_INCLUDED
#define TRIGGER_H_INCLUDED

#define TRIGGER_SIZE     4

#define TRIGGER_IDLE                    0xFF        // Should be non-zero to distinguish it from non-initiated eeprom memory which is zero in eeData
#define TRIGGER_SEND_DATA               0x01
#define TRIGGER_BULK_DATA_RECEIVED      0x02
#define TRIGGER_BULK_TRANSFER_ACTIVE    0x03
#define TRIGGER_PRINT_MEMORY            0x04

#define TRIGGER_BUTTON_A                0x10
#define TRIGGER_BUTTON_B                0x11

#define TRIGGER_NUM_LOCK                0x20
#define TRIGGER_CAPS_LOCK               0x21
#define TRIGGER_SCROLL_LOCK             0x22

extern volatile uint8_t trigger[TRIGGER_SIZE];
extern volatile uint8_t triggerTimeout;         // For how many timer1 cycles of inactivity should be trigger[] reset

void triggerReset();
uint8_t triggerIsEqual(volatile uint8_t* arrA, uint8_t* arrB, uint8_t trigger_length);
void triggerAdd(uint8_t triggerSignal);

#endif // TRIGGER_H_INCLUDED
