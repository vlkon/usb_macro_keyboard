// main.h

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include "main_config.h"

/*----------------------*/
/*--Defines-------------*/
/*----------------------*/

#if DATA_COUNT < 4
#error "Data_count is less than 4 which is minimal size to utilize Num- Caps-lock sensing procedures in main() loop. Risk of undefined behavior in eeprom access."
#endif // DATA_COUNT

/*----------------------*/
/*--Prototypes----------*/
/*----------------------*/
//Enter infinite loop and wait for watchdog timer to reset MCU
void mcuReset(void);

void eepromInit(void);

//Parse values in buffer (of "maxLen" size) and send corresponding key press(-es) to USB HID keyboard interface
//Also implements special functions for buf[] values in range 0x80-0xFF -> so for example a delay can be stored in eeprom data block
//Return 0 success; 1 if the buffer is not properly NULL terminated in the memory
uint8_t parseKeyBuffer(uint8_t* buf, const uint8_t maxLen);

//Function for reading eeprom and sending it to USB keyboard pipe; require address of eeprom data block
void dataSend(const uint8_t address);

//Store received data to proper eeprom memory location
void dataStore(void);

/*----------------------*/
/*--Shared global var.--*/
/*----------------------*/
extern uint8_t buffer[];            //Generic buffer for data handling

typedef struct {
	uint8_t reserved;                       //TODO - dummy memory allocation at location 0 - prevent data corruption when address pointer of eemem gets damaged - maybe not necessary at all (also check eemem file if it is at the first position)
	volatile uint8_t triggerCountTimeout;   //Timeout between each lockkey press
	uint8_t dataSendDelay;          //Delay for user to stop pressing keys then wait this many timer1 loop then process dataN
	uint8_t buttonAllCntr;          //Counter all buttons pressed - for device reset
	uint8_t buttonACntr;            //Counter for how may cycles of timer1 has to be pressed button A to trigger
	uint8_t buttonBCntr;            //Counter for how may cycles of timer1 has to be pressed button B to trigger
} generalConfig_struct_t;

extern generalConfig_struct_t generalConfig;       //Structure holding information for Num-lock send msg trigger event

#endif // MAIN_H_INCLUDED
