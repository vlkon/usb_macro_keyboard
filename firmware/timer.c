
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>        //Resetting watch-dog timer
#include <avr/sleep.h>      //Delay loop - sleep mode has to be defined before delayTimer1 is called, also sei() has to be active

#include "usbdrv/usbdrv.h"
#include "hid.h"

#include "main.h"
#include "timer.h"
#include "trigger.h"
#include "button.h"

volatile uint8_t timeout = 0;   //Timer 1 down counter - used for delay function

//Interrupt driven ~50Hz timer1 to poll USB connection and generaly give info that it is alive.
//ISR_NOBLOCK prevents obscuring of USB protocols
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK)
{
	//wdt_reset();        //Prevent reset from watchdog //moved from timer which is always triggered (original function of WDT was lost?)
	usbPoll();          //Prevent disconnection from USB by polling at min. 20Hz

	//lock-key sensing
	if(triggerTimeout > 0) {
		--triggerTimeout;
	} else {
		triggerReset();
	}

	if(timeout) {--timeout;}

	//Button sensing
    if(!(BUT_PIN & (1 << BUT_A_P)) && !(BUT_PIN & (1 << BUT_B_P))){
        ++buttonAllCntr;
	} else {
        if(buttonAllCntr > generalConfig.buttonAllCntr){
            mcuReset();
        }
        buttonAllCntr = 0;
	}

	if(!(BUT_PIN & (1 << BUT_A_P))){
        if(BUT_PIN & (1 << BUT_B_P)){   //only 1 button has to be pressed at the time
            ++buttonACntr;
        }
	} else {
        if(buttonACntr > generalConfig.buttonACntr){
            trigger[0] = TRIGGER_BUTTON_A;
        }
        buttonACntr = 0;
	}

    if(!(BUT_PIN & (1 << BUT_B_P))){
        if(BUT_PIN & (1 << BUT_A_P)){
            ++buttonBCntr;
        }
	} else {
        if(buttonBCntr > generalConfig.buttonBCntr){
            trigger[0] = TRIGGER_BUTTON_B;
        }
        buttonBCntr = 0;
	}
}

//Init ~50Hz CTC timer with overflow interrupt
void timerInit()
{
    #if F_TIMER1 != 50UL
    #error "CTC timing has to be corrected. It was only designed for 50Hz."
    #endif // F_TIMER1

	OCR1B = 0xFF;       //Set the value to TOP which should not be reached in ctc mode - prevent some misbehavior of COMPB if any
	TIMSK = 0x40;       //Enable COMPA interrupt flags
	TCNT1 = 0x00;       //Reset counter to BOTTOM value
	GTCCR = 0x02;       //Reset prescaler

    #if F_CPU == 12000000UL
	OCR1C = 234;
	TCCR1 = 0x8B;       //Start clock in CTC mode with prescaler 1024
    #elif F_CPU == 16500000UL
	OCR1C = 160;
	TCCR1 = 0x8C;       //Start clock in CTC mode with prescaler 2048
    #else
    #error "Timer not defined for this clock"
    #endif // F_CPU
	OCR1A = OCR1C;      //To trigger interrupt at MAX value of CTC counter
}

//Interrupt driven delay on timer 1; timeout is in timer1 cycles
void delayTimer1(uint8_t cycles)
{
	timeout = cycles;

	//Since USB is probably waking up the device with INT0 interrupt the while(cyles--) can't be used directly for delay
	// timeout is decremented in ISR
	while(timeout) {
		wdt_reset();
		sleep_mode();
	}
}
