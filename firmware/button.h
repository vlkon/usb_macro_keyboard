#ifndef BUTTON_H_INCLUDED
#define BUTTON_H_INCLUDED

#include <stdint.h>
#include <avr/io.h>

#define BUT_A_P     PB0
#define BUT_B_P     PB2

//ATtiny has only B Port so it doesn't have to be
#define BUT_PORT  PORTB
#define BUT_DDR   DDRB
#define BUT_PIN   PINB

extern volatile uint8_t buttonAllCntr;
extern volatile uint8_t buttonACntr;
extern volatile uint8_t buttonBCntr;

void buttonInit();

#endif // BUTTON_H_INCLUDED
