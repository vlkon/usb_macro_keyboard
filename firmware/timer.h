// timer.h
//	Timer1 procedures - generic timeouts and USB polling calls

#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

#define F_TIMER1     50UL          //Timer operating frequency - has to corelate with timerInit values

void timerInit();
void delayTimer1(uint8_t cycles);

#endif // TIMER_H_INCLUDED
