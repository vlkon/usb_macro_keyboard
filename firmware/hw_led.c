#include <avr/io.h>
#include "main_config.h"


//Use hardware led to signalize operation of the device - it is not related to keyboard lock-LEDs
#if HWLED_CONNECTED == 1
#if HWLED_OPERATION_INVERTED == 0
void hwLedInit()      //Set default values to OFF output - low
{
    HWLED_PORT &= ~(1 << HWLED_P);
    HWLED_DDR |= 1 << HWLED_P;
}
void hwLedOn()
{
    HWLED_PORT |= (1 << HWLED_P);
}
void hwLedOff()
{
    HWLED_PORT &= ~(1 << HWLED_P);
}
#else
void hwLedInit()      //Set default values to OFF output - high
{
    HWLED_PORT |= 1 << HWLED_P;
    HWLED_DDR |= 1 << HWLED_P;
}
void hwLedOn()
{
    HWLED_PORT &= ~(1 << HWLED_P);
}
void hwLedOff()
{
    HWLED_PORT |= (1 << HWLED_P);
}
#endif // HWLED_OPERATION_INVERTED
#endif // HWLED_CONNECTED
