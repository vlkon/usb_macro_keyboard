// hid.h

#ifndef HID_H_INCLUDED
#define HID_H_INCLUDED

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

#define KEY_PRESS_DELAY     10      //Default delay between each key press (ms) - too fast and some data may be lost

// Prototypes
void atok(uint8_t ascii);           //ASCII to keyboard_report_t -> store in global variable
void sendKeyPress(uint8_t ascii);   //Send single pres and release key signal - call from main modul instead of sendReport

void sendReport();                  //Flush value of keyboardReport to USB stream. This function should not be directly called from main - use sendKeyPress
void lockDefault();                 //Set default locks - Caps-lock off, Numlock on -> keypress afterwards will provide correct values

// Other prototypes are already in usbdrv.h

// data structure for boot protocol keyboard report
// see HID1_11.pdf appendix B section 1
typedef struct {
	uint8_t modifier;
	uint8_t reserved;
	uint8_t keycode[6];
} keyboard_report_t;

// Global variables
extern keyboard_report_t keyboardReport;
// Reset report structure to default values
#define keyboardReportReset()               \
            keyboardReport.modifier=0;      \
            keyboardReport.reserved=0;      \
            keyboardReport.keycode[0]=0;    \
            keyboardReport.keycode[1]=0;    \
            keyboardReport.keycode[2]=0;    \
            keyboardReport.keycode[3]=0;    \
            keyboardReport.keycode[4]=0;    \
            keyboardReport.keycode[5]=0;

// HID related global variables
extern uint8_t idleRate;            //see HID1_11.pdf sect 7.2.4
extern uint8_t protocolVersion;     //see HID1_11.pdf sect 7.2.6
extern uint8_t ledState;            //see HID1_11.pdf appendix B section 1

// See http://vusb.wikidot.com/driver-api
// Constants are found in usbdrv.h
// Modifier keys masks
#define KB_SHIFT_LEFT       0x02
#define KB_SHIFT_RIGHT      0x20
#define KB_ALT_LEFT         0x04
#define KB_ALT_RIGHT        0x40
#define KB_CTRL_LEFT        0x01
#define KB_CTRL_RIGHT       0x10
#define KB_GUI_LEFT         0x08    //??? - probably left "Window key"
#define KB_GUI_RIGHT        0x80    //??? - probably right "Window key"
#define KB_ALTGR            (KB_ALT_RIGHT)      //Special modifier key 3rd level

// LED mask definition in HID report - see HID1_11.pdf appendix B section 1
#define LEDSTATE_NUM_LOCK           0x01        //1: Num ON
#define LEDSTATE_CAPS_LOCK          0x02        //1: Caps ON
#define LEDSTATE_SCROLL_LOCK        0x04        //1: Scroll ON
#define LEDSTATE_LOCKS_MASK         (LEDSTATE_CAPS_LOCK | LEDSTATE_NUM_LOCK | LEDSTATE_SCROLL_LOCK)


#endif // HID_H_INCLUDED
