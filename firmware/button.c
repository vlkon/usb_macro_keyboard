#include "button.h"

volatile uint8_t buttonAllCntr = 0;
volatile uint8_t buttonACntr = 0;
volatile uint8_t buttonBCntr = 0;

void buttonInit()
{
    BUT_DDR &= ~((1<<BUT_A_P) | (1<<BUT_B_P));  // Redundant - ddr is 0 from restart
    BUT_PORT |= (1<<BUT_A_P) | (1<<BUT_B_P);    //pull-up enable
}
