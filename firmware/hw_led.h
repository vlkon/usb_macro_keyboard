#ifndef HW_LED_H_INCLUDED
#define HW_LED_H_INCLUDED

void hwLedInit();

void hwLedOn();
void hwLedOff();

#endif // HW_LED_H_INCLUDED
